package net.treasurous.crates;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.List;

public class Crate implements Listener {

    private List<ItemStack> loot;
    private FallingBlock chest;
    private Chest crate;

    public Crate(Plugin plugin, Location location, int rarity) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);

        loot = Loot.generateLoot(rarity);
        chest = location.getWorld().spawnFallingBlock(location, Material.CHEST, (byte)0);

    }

    @EventHandler
    public void onLand(EntityChangeBlockEvent e) {
        if (!e.getEntity().equals(chest)) return;

        //Bugs that I'll fix later
        e.setCancelled(true);

        e.getBlock().setType(Material.CHEST);

        crate = (Chest) e.getBlock().getState();

        for (ItemStack i : loot) {
            crate.getBlockInventory().addItem(i);
        }

    }

    @EventHandler
    public void onCrateOpen(InventoryClickEvent e) {
        if (crate == null) return;

        if (!e.getInventory().equals(crate.getBlockInventory())) return;

        if (e.getInventory().firstEmpty() != 0) return;

        crate.getBlock().setType(Material.AIR);

        HandlerList.unregisterAll(this);

    }

}
