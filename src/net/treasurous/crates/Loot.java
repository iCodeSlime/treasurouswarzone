package net.treasurous.crates;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Loot {

    //If I find a use for making Loot object oriented
    public Loot(int rarity) {
    }

    //Current method
    public static List<ItemStack> generateLoot(int rarity) {
        List<ItemStack> dab = new ArrayList<>();
        for (LootItem loot : loot) {
            Random random = new Random();
            int bg = 1;
            int chance = random.nextInt(loot.getRarity());
            if (chance >= rarity) continue;
            int w = 0;
            while(w < 1) {
                chance = random.nextInt(loot.getRarity());
                if (chance < rarity) {
                    bg++;
                    Bukkit.broadcastMessage(bg + "");
                    rarity = rarity/2;
                }
                else w++;
            }
            dab.add(loot.createItem(bg));
        }
        return dab;
    }

    private static List<LootItem> loot = new ArrayList<>();

    //Ignore the random names, I'm changing them later. Just for testing purposes.
    static {

        //PARADOXICAL 1000+
        loot.add(new LootItem(Material.SPIDER_EYE, 1041, "Cave's Eye"));

        //MYTHICALS 500+
        loot.add(new LootItem(Material.DRAGON_EGG, 500, "The Egg of Life"));
        loot.add(new LootItem(Material.DIAMOND_BLOCK, 500, "Mah Nama Chegg"));

        //RARES 100+
        loot.add(new LootItem(Material.GOLD_BLOCK, 100, "GOLD BLOCK WOOO"));
        loot.add(new LootItem(Material.IRON_BLOCK, 100, "Why do I exist"));

        //UNCOMMONS 20+
        loot.add(new LootItem(Material.DIAMOND, 20, "fake diamond ez"));
        loot.add(new LootItem(Material.GOLD_INGOT, 20, "o _o"));

        //COMMONS 10+
        loot.add(new LootItem(Material.APPLE, 10, "bro don't eat me"));
        loot.add(new LootItem(Material.COOKED_BEEF, 10, "PLEASE EAT ME"));
    }


}