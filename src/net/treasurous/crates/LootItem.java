package net.treasurous.crates;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class LootItem {

    private Material material;
    private int rarity;
    private String name;

    public LootItem(Material material, int rarity, String name) {
        this.material = material;
        this.rarity = rarity;
        this.name = name;
    }

    public Material getMaterial() {
        return material;
    }

    public int getRarity() {
        return rarity;
    }

    public String getDisplayName() {
        return name;
    }

    public ItemStack createItem(int r) {
        ItemStack item = new ItemStack(material, r);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        item.setItemMeta(meta);
        return item;
    }

}
