package net.treasurous;

import net.treasurous.command.Warcrate;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class TreasurousWarzone extends JavaPlugin {

    private static Plugin plugin;

    @Override
    public void onEnable() {
        plugin = this;

        getCommand("warcrate").setExecutor(new Warcrate());

    }

    public static Plugin getPlugin() {
        return plugin;
    }
}
