package net.treasurous.command;

import net.treasurous.TreasurousWarzone;
import net.treasurous.crates.Crate;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Warcrate implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String string, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        Location loc = p.getLocation();
        loc.add(1, 4, 0);

        new Crate(TreasurousWarzone.getPlugin(), loc, Integer.parseInt(args[0]));

        return true;
    }

}
